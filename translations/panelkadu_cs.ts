<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs">
<context>
    <name>@default</name>
    <message>
        <source>Look</source>
        <translation>Vzhled</translation>
    </message>
    <message>
        <source>Panel</source>
        <translation>Panel</translation>
    </message>
    <message>
        <source>Geometry</source>
        <translation>Uspořádání</translation>
    </message>
    <message>
        <source>Side</source>
        <translation>Strana</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>Vpravo</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation>Dole</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>Vlevo</translation>
    </message>
    <message>
        <source>Top</source>
        <translation>Nahoře</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Velikost</translation>
    </message>
    <message>
        <source> px</source>
        <translation> px</translation>
    </message>
    <message>
        <source>User defined panel length</source>
        <translation>Uživatelem stanovená délka panelu</translation>
    </message>
    <message>
        <source>Position</source>
        <translation>Poloha</translation>
    </message>
    <message>
        <source>Length</source>
        <translation>Délka</translation>
    </message>
    <message>
        <source>Behaviour</source>
        <translation>Chování</translation>
    </message>
    <message>
        <source>Activation time</source>
        <translation>Čas spuštění</translation>
    </message>
    <message numerus="yes">
        <source>%n millisecond(s)</source>
        <translation>
            <numerusform>%n milisekunda</numerusform>
            <numerusform>%n milisekundy</numerusform>
            <numerusform>%n milisekund</numerusform>
        </translation>
    </message>
    <message>
        <source>Hide panel after</source>
        <translation>Skrýt panel po</translation>
    </message>
    <message numerus="yes">
        <source>[after] %n millisecond(s)</source>
        <translation>
            <numerusform>[po] %n milisekundě</numerusform>
            <numerusform>[po] %n milisekundách</numerusform>
            <numerusform>[po] %n milisekundách</numerusform>
        </translation>
    </message>
    <message>
        <source>Hiding margin</source>
        <translation>Skrytí okraje</translation>
    </message>
    <message>
        <source>Use activation ranges</source>
        <translation>Použít spouštěcí rozsahy</translation>
    </message>
    <message>
        <source>Activation ranges</source>
        <translation>Spouštěcí rozsahy</translation>
    </message>
    <message>
        <source>Use spaces to separate ranges: &apos;0-100 200-300 400-500&apos;</source>
        <translation>Použít mezery pro oddělení rozsahů: &apos;0-100 200-300 400-500&apos;</translation>
    </message>
    <message>
        <source>Don&apos;t hide panel when active</source>
        <translation>Neskrývat panel při činnosti</translation>
    </message>
</context>
</TS>
