<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>@default</name>
    <message>
        <source>Look</source>
        <translation>Look</translation>
    </message>
    <message>
        <source>Panel</source>
        <translation>Panel</translation>
    </message>
    <message>
        <source>Geometry</source>
        <translation>Geometry</translation>
    </message>
    <message>
        <source>Side</source>
        <translation>Side</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>Right</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation>Bottom</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>Left</translation>
    </message>
    <message>
        <source>Top</source>
        <translation>Top</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Size</translation>
    </message>
    <message>
        <source> px</source>
        <translation> px</translation>
    </message>
    <message>
        <source>User defined panel length</source>
        <translation>User defined panel length</translation>
    </message>
    <message>
        <source>Position</source>
        <translation>Position</translation>
    </message>
    <message>
        <source>Length</source>
        <translation>Length</translation>
    </message>
    <message>
        <source>Behaviour</source>
        <translation>Behaviour</translation>
    </message>
    <message>
        <source>Activation time</source>
        <translation>Activation time</translation>
    </message>
    <message numerus="yes">
        <source>%n millisecond(s)</source>
        <translation>
            <numerusform>%n millisecond</numerusform>
            <numerusform>%n milliseconds</numerusform>
        </translation>
    </message>
    <message>
        <source>Hide panel after</source>
        <translation>Hide panel after</translation>
    </message>
    <message numerus="yes">
        <source>[after] %n millisecond(s)</source>
        <translation>
            <numerusform>[after] %n millisecond</numerusform>
            <numerusform>[after] %n milliseconds</numerusform>
        </translation>
    </message>
    <message>
        <source>Hiding margin</source>
        <translation>Hiding margin</translation>
    </message>
    <message>
        <source>Use activation ranges</source>
        <translation>Use activation ranges</translation>
    </message>
    <message>
        <source>Activation ranges</source>
        <translation>Activation ranges</translation>
    </message>
    <message>
        <source>Use spaces to separate ranges: &apos;0-100 200-300 400-500&apos;</source>
        <translation>Use spaces to separate ranges: &apos;0-100 200-300 400-500&apos;</translation>
    </message>
    <message>
        <source>Don&apos;t hide panel when active</source>
        <translation>Don&apos;t hide panel when active</translation>
    </message>
</context>
</TS>
