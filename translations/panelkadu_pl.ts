<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <source>Look</source>
        <translation>Wygląd</translation>
    </message>
    <message>
        <source>Panel</source>
        <translation>Panel</translation>
    </message>
    <message>
        <source>Geometry</source>
        <translation>Geometria</translation>
    </message>
    <message>
        <source>Side</source>
        <translation>Strona</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>Prawo</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation>Dół</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>Lewo</translation>
    </message>
    <message>
        <source>Top</source>
        <translation>Góra</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Rozmiar</translation>
    </message>
    <message>
        <source> px</source>
        <translation> px</translation>
    </message>
    <message>
        <source>User defined panel length</source>
        <translation>Własna długość panelu</translation>
    </message>
    <message>
        <source>Position</source>
        <translation>Pozycja</translation>
    </message>
    <message>
        <source>Length</source>
        <translation>Długość</translation>
    </message>
    <message>
        <source>Behaviour</source>
        <translation>Zachowanie</translation>
    </message>
    <message>
        <source>Activation time</source>
        <translation>Czas aktywacji</translation>
    </message>
    <message numerus="yes">
        <source>%n millisecond(s)</source>
        <translation>
            <numerusform>%n milisekunda</numerusform>
            <numerusform>%n milisekundy</numerusform>
            <numerusform>%n milisekund</numerusform>
        </translation>
    </message>
    <message>
        <source>Hide panel after</source>
        <translation>Ukryj panel po</translation>
    </message>
    <message numerus="yes">
        <source>[after] %n millisecond(s)</source>
        <translation>
            <numerusform>[po] %n milisekundzie</numerusform>
            <numerusform>[po] %n milisekundach</numerusform>
            <numerusform>[po] %n milisekundach</numerusform>
        </translation>
    </message>
    <message>
        <source>Hiding margin</source>
        <translation>Margines ukrywania</translation>
    </message>
    <message>
        <source>Use activation ranges</source>
        <translation>Używaj przedziałów aktywacji</translation>
    </message>
    <message>
        <source>Activation ranges</source>
        <translation>Przedziały aktywacji</translation>
    </message>
    <message>
        <source>Use spaces to separate ranges: &apos;0-100 200-300 400-500&apos;</source>
        <translation>Użyj spacji do rozdzielenia przedziałów: &apos;0-100 200-300 400-500&apos;</translation>
    </message>
    <message>
        <source>Don&apos;t hide panel when active</source>
        <translation>Nie ukrywaj panelu gdy aktywny</translation>
    </message>
</context>
</TS>
