<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>@default</name>
    <message>
        <source>Look</source>
        <translation>Aspecto</translation>
    </message>
    <message>
        <source>Panel</source>
        <translation>Panel</translation>
    </message>
    <message>
        <source>Geometry</source>
        <translation>Geometría</translation>
    </message>
    <message>
        <source>Side</source>
        <translation>Lado</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>Derecho</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation>Parte inferior</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>Izquierda</translation>
    </message>
    <message>
        <source>Top</source>
        <translation>Comienzo de la página</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <source> px</source>
        <translation>px</translation>
    </message>
    <message>
        <source>User defined panel length</source>
        <translation>Longitud del panel definido por el usuario </translation>
    </message>
    <message>
        <source>Position</source>
        <translation>Posición</translation>
    </message>
    <message>
        <source>Length</source>
        <translation>Longitud</translation>
    </message>
    <message>
        <source>Behaviour</source>
        <translation>Comportamiento</translation>
    </message>
    <message>
        <source>Activation time</source>
        <translation>Tiempo de activación</translation>
    </message>
    <message numerus="yes">
        <source>%n millisecond(s)</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <source>Hide panel after</source>
        <translation>Ocultar panel después</translation>
    </message>
    <message numerus="yes">
        <source>[after] %n millisecond(s)</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <source>Hiding margin</source>
        <translation>Ocultar margen</translation>
    </message>
    <message>
        <source>Use activation ranges</source>
        <translation>Usar rangos de activación</translation>
    </message>
    <message>
        <source>Activation ranges</source>
        <translation>Activación de los rangos</translation>
    </message>
    <message>
        <source>Use spaces to separate ranges: &apos;0-100 200-300 400-500&apos;</source>
        <translation>Use espacios para separar los rangos: 0 -100 200-300 400-500 &apos;</translation>
    </message>
    <message>
        <source>Don&apos;t hide panel when active</source>
        <translation>No ocultar el panel cuando está activo</translation>
    </message>
</context>
</TS>
